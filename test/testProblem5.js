const inventory = require('./inventory');
const problem4 = require('../problem4');
const problem5 = require('../problem5');

const allCarYears = problem4(inventory);

console.log(problem5(allCarYears));
console.log(problem5(allCarYears).length);