/*The marketing team wants the car models listed alphabetically on the website. 
Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.*/

const sortCarModels = function(inventory) {
    let carModelArr = [];

    for(let i = 0; i < inventory.length; i++){

        carModelArr.push(inventory[i].car_model.toUpperCase().trim()); // to comapare and sort them we have to first convert all car models into either upper case or lower case
                                                                       // using toUpperCase() or toLowerCase() methods.
                                                                       // some car models may have spaces at the beginning or at the end , so we have to trim them using .trim() method.
    }

    return carModelArr.sort();
}

module.exports = sortCarModels;