/*The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
Then log the car's year, make, and model in the console log in the format of:*/

const findById = function(inventory) { // created a function with a variable called result

    for (let i = 0; i < inventory.length; i++){ // using for-loop to iterate through the array. 

        if(inventory[i].id == 33) { // checking the id, if id matches with 33 then it will return. 

            return(`Car 33 is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`);

        }

    }

}

module.exports = findById; //exporting the function.