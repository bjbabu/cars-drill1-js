/*The car lot manager needs to find out how many cars are older than the year 2000. 
Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.*/

const olderCars = function(arr) {
    let olderCarsArr = [];

    for(let i = 0; i < arr.length; i++){
        if(arr[i] < 2000) {
            olderCarsArr.push(arr[i]);
        }
    }

    return olderCarsArr;
}

module.exports = olderCars;