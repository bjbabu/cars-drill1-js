/*The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  
Log the make and model into the console in the format of:*/ 

const lastCar = function(inventory) {

    let n = inventory.length;

    let requiredCar = inventory[n-1];

    return(`Last car is a ${requiredCar.car_make} ${requiredCar.car_model}`);
}

module.exports = lastCar;